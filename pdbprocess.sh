#!/bin/bash

#Usage: pdbprocess.sh [name of directory in the pdb folder]

#Get the pdb directory and make likewise directories in hb2, nb2, and dssr.
homedir=$(pwd)
pdbdir=$homedir'/pdb/'$1
datadir=$homedir'/datafiles/'$1
hbplus=./hbplus
HDA=${2:-3.35} #Hbond D-A (-d) 
HHA=${3:-2.70} #Hbond H-A (-h)
VDA=${4:-3.90} #VdW D-A (-d) 
VHA=${5:-3.90} #VdW H-A (-h)

dssr=./x3dna-dssr

#Make sure that the pdb folder exists
if [ ! -d $pdbdir ]; then
	echo 'PDB folder does not exist. Quiting.'
	exit
fi
#Make the folder, if it doesn't exist 
if [ ! -d $datadir ]; then
	mkdir $datadir
fi


#First make an info text file, then run through every file and run hbplus and dssr
echo '=== PROCESSING INFO ===' > $datadir'/info.txt'
echo 'HBplus Options:' >> $datadir'/info.txt'
echo 'HBond: D-A (-d): '$HDA' | H-A (-h): '$HHA >> $datadir'/info.txt'
echo 'VdW  : D-A (-d): '$VDA' | H-A (-h): '$VHA >> $datadir'/info.txt'


echo 'Complexes:' >> $datadir'/info.txt'
for file in $pdbdir/*
do
	complex=${file%%'.pdb'}
	complex=${complex##$pdbdir'/'}
	complexdir=$datadir'/'$complex
	echo $complex >> $datadir'/info.txt'
	

	mkdir $complexdir
	$hbplus -n -h $HHA -d $HDA "$file" 
	$hbplus -N -h $VHA -d $VDA "$file" 
	
       	mv -f $homedir'/'$complex'.hb2' $complexdir
       	mv -f $homedir'/'$complex'.nb2' $complexdir
	
	$dssr -i="$file" --more=true -o="$complex".json -json 
       	mv -f $homedir'/'$complex'.json' $complexdir
done

#Moves the extra files that dssr creates.
mkdir $datadir'/dssr-extra'
mv -f $homedir'/dssr-'* $datadir'/dssr-extra'
mv -f $homedir'/hbdebug.dat' $datadir
